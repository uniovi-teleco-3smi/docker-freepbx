# Contenedores con centralita VoIP Asterisk para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modifícalo según tus preferencias.

### Parámetros que pueden modificarse

 * No se recomienda cambiar ninguno.

Los parámetros deben cambiarse en el fichero 'docker-compose.yml' antes de iniciar el contenedor.

### Arrancar los contenedores

Para arrancar los contenedores:

```console
sudo docker-compose up
```

Para parar/arrancar/reiniciar/destruir los contenedores:

```console
sudo docker-compose stop/start/restart/down
```

Los contenedores volverán a arrancar automáticamente si se reinicia la máquina anfitriona.
